export const search = (callback) => $(document).on("click", "#search-button", callback);
export const random = (callback) => $(document).on("click", "#show-random-gif", callback);
export const trending = (callback) => $(document).on("click", "#show-trending", callback);

export const detailsPopUp = (callback) =>$(document).on('click', '[data-id]', callback);

export const upload = (callback) => $(document).on("click", "#submit", callback);
export const showUploads = (callback) => $(document).on("click", "#show-uploads", callback);
export const showSomeMore = (callback) => $(document).on("click", "#loadMore", callback);
