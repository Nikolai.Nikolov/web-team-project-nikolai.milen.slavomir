import { showSearchResult, showRandom, showTrending, submit, getUploads, callThem } from "./views.js"
import { search, trending, random, upload, showUploads, showSomeMore } from "./events.js"

(() => {
    trending(showTrending)
    random(showRandom)
    search(showSearchResult)
    upload(submit);
    showUploads(getUploads);
    showSomeMore(callThem)
})()