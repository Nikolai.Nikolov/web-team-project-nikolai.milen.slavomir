/*eslint-env jquery*/

import { ids } from "./database.js"

export const showSearchResult = async () => {
    const $container = $("#container")
    const $info = $("#info")
    $container.empty()
    $("#container2").empty()
    $info.empty()
    const inputed = $("#search-input").val().split(' ').join('+')
    let fetchData = await fetch(`http://api.giphy.com/v1/gifs/search?q=${inputed}&api_key=WrJ7ucpQ7WbgvsMJXpa5o27kKKmKIJi5&limit=20$rating:g`);
    let result = await fetchData.json();
   
    // let info2 = result.data.title;
    let data = result.data.forEach(currentGif => {
        $("#container").append(`<img class="gif" src="${currentGif.images.downsized_large.url}" " title="User: ${currentGif.username}  Title:${currentGif.title}">`)
        // $("#info").append(`<span>Uploaded by: ${currentGif.username}</span>`)
    });
};

export const showRandom = async () => {
    const $container = $("#container")
    const $info = $("#info")
    $("#container2").empty()
    $container.empty()
    $info.empty()
    let fetchData = await fetch("https://api.giphy.com/v1/gifs/random?api_key=WrJ7ucpQ7WbgvsMJXpa5o27kKKmKIJi5");
    let result = await fetchData.json();
    let info = result.data.username;
    let info2 = result.data.title;
    let url = result.data.images.downsized_large.url

    $("#container").append(`<img class="gif" src="${url}" title="User: ${info}  Title:${info2}" >`)
    // $info.append(`<span>Uploaded by: ${info}</span>`)

};

export const showTrending = async () => {
    const $container = $("#container")
    const $info = $("#info")
    $("#container2").empty()
    $container.empty()
    $info.empty()
    let fetchData = await fetch("https://api.giphy.com/v1/gifs/trending?api_key=WrJ7ucpQ7WbgvsMJXpa5o27kKKmKIJi5&limit=20"); //"http://api.giphy.com/v1/gifs/search?q=ryan+gosling&api_key=YOUR_API_KEY&limit=5");
    let result = await fetchData.json();
    // let info2 = result.data.username
    // let info3 = result.data.title
    let data = result.data.forEach(currentGif => {
        $("#container").append(`<img class="gif" src="${currentGif.images.downsized_large.url}" title="User: ${currentGif.username}  Title:${currentGif.title}">`)
        // $("#info").append(`<span>Uploaded by: ${currentGif.username}</span>`)
    })
};



export const submit = async (e) => {
    e.preventDefault()
    const fileInput = $('#file-to-upload')
    const formData = new FormData();
    // console.log(fileInput.get(0).files[0])
    formData.append("file", fileInput.get(0).files[0])
    // console.log(formData.get(0))
    const options = {
        method: "POST",
        body: formData,
    }

    let fetchData = await fetch(`https://upload.giphy.com/v1/gifs?api_key=WrJ7ucpQ7WbgvsMJXpa5o27kKKmKIJi5`, options)
    let json = await fetchData.json()
    
    ids.push(json.data.id)
    localStorage.setItem("id", ids)
    


}

export const getUploads = async (e) => {
    e.preventDefault()
    const $container = $("#container")
    const $info = $("#info")
    $container.empty()
    $info.empty()
    


    let fetchData = await fetch(`http://api.giphy.com/v1/gifs?api_key=WrJ7ucpQ7WbgvsMJXpa5o27kKKmKIJi5&ids=${localStorage.getItem("id")}`);
    let result = await fetchData.json();
    let data = result.data;
    // let info2 = result.data.username
    // let info3 = result.data.title

    data.forEach(currentGif => {
        $container.append(`<img class="gif" src=${currentGif.images.downsized_large.url} title="User: ${currentGif.username}  Title:${currentGif.title}">`)
        // $("#info").append(`<span>Uploaded by: ${currentGif.username}</span>`)
        $(".gif").css({ "margin": "8px", 'border': '3px solid grey', 'transition': 'transform .2s' })
    })

}

export const more = async () => {
    const $container = $("#container2")
    const $info = $("#info-2")
    // $container.empty()
    $info.empty()
    let fetchData = await fetch("https://api.giphy.com/v1/gifs/random?api_key=WrJ7ucpQ7WbgvsMJXpa5o27kKKmKIJi5");
    let result = await fetchData.json();

    let url = result.data.images.downsized_large.url
    let info2 = result.data.username
    let info3 = result.data.title

    $("#container2").append(`<img class="gif" src="${url}" title="User: ${info2}  Title:${info3}" >`)

};

export const callThem = async () => {
    more()
    more()
    more()
    more()
    more()
}
